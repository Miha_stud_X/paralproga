#include "potocs.h"

int push_queue(Node_t **head, T value){
    Node_t *tmp = (Node_t *)malloc(sizeof(Node_t));
	if (tmp == NULL) {
		return 0;
    }
    tmp->next = NULL;
    tmp->value = value;
    if(*head){
		Node_t *buf_head = *head;
		while(buf_head->next != NULL){
			buf_head = buf_head->next;
		}
		buf_head->next = tmp;
	}else{
		*head = tmp;
	}
    return 1;
}

int pop(Node_t **head){
    if ((*head) == NULL) {
        return 0;
    }
    Node_t *val = *head;
    *head = (*head)->next;
    free(val);
    return 1;
}

T peek(const Node_t* head){
    return head->value;
}

T peek(const Node_a* head){
    return head->value.load();
}

int pop(Node_a **head){
    if (*head == NULL) {
        return 0;
    }
    Node_a *val = *head;
    *head = (*head)->next;
    free(val);
    return 1;
}

int push_queue(Node_a **head, T value){
    Node_a *tmp = (Node_a *)malloc(sizeof(Node_a));
	if (tmp == NULL) {
		return 0;
    }
    tmp->next = NULL;
    tmp->value = value;
    if(*head){
		Node_a *buf_head = *head;
		while(buf_head->next != NULL){
			buf_head = buf_head->next;
		}
		buf_head->next = tmp;
	}else{
		*head = tmp;
	}
    return 1;
}
