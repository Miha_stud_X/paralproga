#include <cstdio>
#include <iostream>
#include <cmath>
#include <ctime>

typedef struct Part_of_the_interval{
	double start;
	double sum;
	double step;
	int count_step;
}p_interval;

void menu(double*, double*, int*, int*);
int integration(double, double, int, int);