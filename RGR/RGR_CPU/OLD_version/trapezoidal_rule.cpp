#include "integration.h"

//функция расчёта заданной функции на куске (метод трапеций)
void *trapeziodal(void *ptc){
	p_interval *ptc1 = (p_interval *)ptc;
	for(int i = 0; i< ptc1->count_step; i++)
	{
		if(i==0 || i == (ptc1->count_step)-1) {//крайние значения
			ptc1->sum += 0.5*(sqrt(ptc1->start)+(ptc1->start)*sin(ptc1->start)+2);
		}else{
			ptc1->sum += sqrt(ptc1->start)+(ptc1->start)*sin(ptc1->start)+2;
		}
		ptc1->start+=ptc1->step;
	}
	ptc1->sum*=ptc1->step;
	return 0;
}

int integration(double start, double end, int n_thread, int count_step){
	
	pthread_t tid[n_thread];//массив tid для потоков
	p_interval mass[n_thread] = {0,0,0,0};//массив кусков для потоков
	double step = (end-start)/count_step;//размер шага
	
	//Запись времени начала работы
    struct timespec t_start, t_end; 
    long int delta_t;
	clock_gettime(CLOCK_REALTIME, &t_start);

	//распределение интервалов исходного промежутка
	int n = (int)(count_step/n_thread);
	for (int i = 0; i< n_thread; i++){
		mass[i].count_step = n;
		mass[i].step = step;
	}
	count_step%=n_thread;
	n = 0;
	while(count_step > 0){
		mass[n++].count_step++;
		count_step--;
	}

	//запись начальной границы в каждый кусок
	mass[0].start = start;
	for(int i = 1; i<n_thread; i++){
		mass[i].start = mass[i-1].start + mass[i-1].count_step*step;
	}
	//создание потоков
	for (int i = 0; i<n_thread; i++){
		pthread_create(&tid[i], NULL, trapeziodal, &mass[i]);
	}

	double all_sum=0;
	//закрытие потоков(привязка)
	for (int i = 0; i<n_thread; i++){
		pthread_join(tid[i], NULL);
		all_sum+=mass[i].sum;
	}
	
	clock_gettime(CLOCK_REALTIME, &t_end);//Определяем текущее время
	//Рассчитываем разницу времени между двумя измерениями
	delta_t=1000000000*(t_end.tv_sec - t_start.tv_sec)+(t_end.tv_nsec - t_start.tv_nsec);
	
	//Выводим результат расчета на экран
	std::cout << "Result: "<< all_sum << std::endl;
	std::cout << "Time: "<< delta_t << "ns" << std::endl;
	return 0;
}
