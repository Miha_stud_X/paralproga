
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <stdio.h>
#include <cmath>
#include <iostream>
#include <ctime>

#define CUDA_DEBUG

#ifdef CUDA_DEBUG
#define CUDA_CHECK_ERROR(err) \
if(err != cudaSuccess){\
 std::cout << "Cuda error: " << cudaGetErrorString(err) << std::endl;\
 std::cout << "Error in line: " << __LINE__ << std::endl;}
#else
#define CUDA_CHECK_ERROR(err)
#endif

__host__ void menu(float *, float *, int *, int*);
__host__ int integration(float, float, int, int);
__global__ void trapeziodal(int *, float *, float, float *);
__global__ void trapeziodal(int *, float *, float, float *);
__global__ void reduce(float *, int);
__device__ float function(float);

__host__ int main(int argc, char ** argv) {
	float start, end;
	int n_thread, steps;
	if (argc == 5) {
		start = atof(argv[1]);
		end = atof(argv[2]);
		steps = std::atoi(argv[3]);
		n_thread = std::atoi(argv[4]);
		if (end <= start || n_thread<1 || steps < n_thread)
		{
			std::cout << "Error arguments!" << std::endl;
		}
	}
	else {
		menu(&start, &end, &n_thread, &steps);
	}
	integration(start, end, n_thread, steps);
	system("pause");
	return 0;
}

__host__ void menu(float *start, float *end, int *n_thread, int*steps) {
	std::cout << "Enter the begining and end of the integration interval." << std::endl;
	while (1) {
		std::cout << "Input start:" << std::endl;
		std::cin >> *start;
		std::cout << "Input end:" << std::endl;
		std::cin >> *end;
		if (*end>*start) break;
		else std::cout << "Wrong interval!" << std::endl;
	}
	while (1) {
		std::cout << "Input count steps:" << std::endl;
		std::cin >> *steps;
		if (*steps>0) break;
	}
	while (1) {
		std::cout << "Input count threads:" << std::endl;
		std::cin >> *n_thread;
		if (*n_thread>0) break;
	}
}

__host__ int integration(float start, float end, int n_thread, int count_steps){
	
	float cuda_time; //����� ������ cuda
	float step = (end - start) / count_steps; //������ ����
	int n = (int)count_steps / n_thread;//����� ����� �� �����
	int ost = count_steps%n_thread;//������� ����� ��� ������������
	int n_cudaBlock;
	int n_cudaThread = 512;
	
	int n_sumThread;//���������� ������� 2 (�� ������ ����� �������)
	for (n_sumThread = 1; n_sumThread < n_thread; n_sumThread *= 2);
	float *host_start = (float*)malloc(n_sumThread * sizeof(float));
	float *host_result = (float*)malloc(n_sumThread * sizeof(float));
	int   *host_count_steps = (int*)malloc(n_sumThread * sizeof(int));
	float *device_start;
	float *device_result;
	int *device_count_steps;

	//������������� ���������� ��������� ����������
	for (int i = 0; i < n_sumThread; i++) {
		host_count_steps[i] = n;
		if (i > n_thread - 1)
		{
			host_start[i] = 0;
			host_count_steps[i] = 0;
			continue;
		}
		if (ost > 0) {
			host_count_steps[i]++;
			ost--;
		}
		if (i == 0) host_start[0] = start;
		else host_start[i] = host_start[i - 1] + host_count_steps[i - 1] * step;
		host_result[i] = 0;
	}

	if (n_sumThread >= n_cudaThread){
		n_cudaBlock = n_sumThread / n_cudaThread;
	}else{
		n_cudaBlock = 1;
		n_cudaThread = n_sumThread;
	}

	cudaEvent_t event_start, event_stop, event_cuda;
	CUDA_CHECK_ERROR(cudaEventCreate(&event_start));
	CUDA_CHECK_ERROR(cudaEventCreate(&event_stop));
	CUDA_CHECK_ERROR(cudaEventCreate(&event_cuda));
	CUDA_CHECK_ERROR(cudaEventRecord(event_start, 0));//������ ������� ������ ������ cuda

	CUDA_CHECK_ERROR(cudaMalloc((void**)&device_start, n_sumThread * sizeof(float)));
	CUDA_CHECK_ERROR(cudaMalloc((void**)&device_result, n_sumThread * sizeof(float)));
	CUDA_CHECK_ERROR(cudaMalloc((void**)&device_count_steps, n_sumThread * sizeof(int)));
	
	CUDA_CHECK_ERROR(cudaMemcpy(device_start, host_start, n_sumThread * sizeof(float), cudaMemcpyHostToDevice));
	CUDA_CHECK_ERROR(cudaMemcpy(device_result, host_result, n_sumThread * sizeof(float), cudaMemcpyHostToDevice));
	CUDA_CHECK_ERROR(cudaMemcpy(device_count_steps, host_count_steps, n_sumThread * sizeof(int), cudaMemcpyHostToDevice));
	trapeziodal << < n_cudaBlock, n_cudaThread >> >(device_count_steps, device_start, step, device_result);//�������� ������ �������� �� �������
	CUDA_CHECK_ERROR(cudaEventSynchronize(event_cuda));//�������������
	CUDA_CHECK_ERROR(cudaMemcpy(host_result, device_result, n_sumThread * sizeof(float), cudaMemcpyDeviceToHost));
	for (int s = 1; s < n_cudaBlock*n_cudaThread; s *= 2)
	{
		reduce << < n_cudaBlock, n_cudaThread >> >(device_result, s);
	}
	CUDA_CHECK_ERROR(cudaEventSynchronize(event_cuda));//�������������
	CUDA_CHECK_ERROR(cudaMemcpy(host_result, device_result, n_sumThread * sizeof(float), cudaMemcpyDeviceToHost));
	
	CUDA_CHECK_ERROR(cudaEventRecord(event_stop, 0));//������ ������� ����� ������ cuda
	CUDA_CHECK_ERROR(cudaEventSynchronize(event_stop));//�������������
	CUDA_CHECK_ERROR(cudaEventElapsedTime(&cuda_time, event_start, event_stop));
	
	std::cout << "Result: " << host_result[0] << std::endl;
	std::cout << "CUDA time: " << cuda_time << "ms" << std::endl;

	//������ ������
	free(host_result);
	free(host_start);
	free(host_count_steps);
	CUDA_CHECK_ERROR(cudaFree(device_result));
	CUDA_CHECK_ERROR(cudaFree(device_start));
	CUDA_CHECK_ERROR(cudaFree(device_count_steps));
	CUDA_CHECK_ERROR(cudaEventDestroy(event_cuda));
	CUDA_CHECK_ERROR(cudaEventDestroy(event_start));
	CUDA_CHECK_ERROR(cudaEventDestroy(event_stop));

	return 0;
}

__global__ void trapeziodal(int *count_step, float *start, float step, float *summ) {
	int index = blockIdx.x*blockDim.x + threadIdx.x;
	float local_sum = 0;
	float local_start = start[index];
	int local_count_step = count_step[index];

	for (int i = 1; i <= local_count_step; i++) {
		if (i == 0 || i == local_count_step - 1) {
			local_sum += 0.5 * function(local_start);
		}
		else {
			local_sum += function(local_start);
		}
		local_start += step;
	}
	summ[index] = local_sum*step;
}

__device__ float function(float x) {
	return sqrtf(x) + x*sinf(x) + 2;
}

__global__ void reduce(float *g_idata, int s) {
	int index = blockIdx.x*blockDim.x + threadIdx.x;
	if (index % (2 * s) == 0) {
		g_idata[index] += g_idata[index + s];
	}
	__syncthreads();
}

