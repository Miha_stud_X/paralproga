#include "integration.h"

#define TIME
#ifdef TIME
	#define SPEED_TEST
	#ifdef SPEED_TEST
		//#define FILE_OUT_TIME
		#define SPEED_COUNT_TEST 105
	#endif
#endif
T step;

T function(T x){
	return sqrtf(x)+x*sinf(x)+2;
}

//функция расчёта заданной функции на куске (метод трапеций)
void *trapeziodal(void *ptc){
	p_interval *ptc1 = (p_interval *)ptc;
	T sum = 0.0;
	T start = ptc1->start;
	int count_step = ptc1->count_step;

	for(int i = 0; i < count_step; i++)
	{
		if(i == 0 || i == count_step-1) {//крайние значения
			sum += 0.5*function(start);
		}else{
			sum += function(start);
		}
		start += step;
	}
	ptc1->sum = sum * step;
	return 0;
}

int integration(T start, T end, int n_thread, int count_step){

#ifdef SPEED_TEST
	T sum_time = 0;
	for (int s = 0; s < SPEED_COUNT_TEST; s++){
#endif
	pthread_t tid[n_thread];//массив tid для потоков
	p_interval mass[n_thread] = {0.0,0.0,0};//массив кусков для потоков
	step = (end-start)/count_step;//размер шага
	int n_step = (int)(count_step/n_thread);//число шагов на поток
	int ost = count_step%n_thread;//остаток шагов для распределения
	T all_sum=0;

#ifdef TIME //Запись времени начала работы
	struct timespec t_start, t_end;
	T delta_t;
	clock_gettime(CLOCK_MONOTONIC, &t_start);
#endif

	for (int i = 0; i<n_thread; i++){
		mass[i].count_step = n_step;
		if(ost>0){
			mass[i].count_step++;
			ost--;
		}
		if (i==0) mass[0].start = start;
		else mass[i].start = mass[i-1].start + mass[i-1].count_step*step;
		pthread_create(&tid[i], NULL, trapeziodal, &mass[i]);
	}

	for (int i = 0; i<n_thread; i++){
		pthread_join(tid[i], NULL);
		all_sum+=mass[i].sum;
	}

#ifdef TIME
	clock_gettime(CLOCK_MONOTONIC, &t_end);
	delta_t=1000*(t_end.tv_sec-t_start.tv_sec)+(t_end.tv_nsec-t_start.tv_nsec)/1000000;
#endif

#ifdef SPEED_TEST
	if(s>4)
		sum_time += delta_t;
	}
#endif

#ifdef FILE_OUT_TIME
	FILE *file;
	file = fopen("out.txt", "at+");
	fprintf(file, "%d: %f\n", n_thread, sum_time/SPEED_COUNT_TEST);
	fclose(file);
#elif SPEED_TEST
	std::cout << "Average time: " << sum_time/SPEED_COUNT_TEST << std::endl;
#endif

#ifndef SPEED_TEST
	std::cout << "Time: "<< delta_t << " ms" << std::endl;
	std::cout << "Result: "<< all_sum << std::endl;
#endif
	return 0;
}
