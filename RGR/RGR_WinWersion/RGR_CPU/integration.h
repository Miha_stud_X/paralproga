#include <cstdio>
#include <iostream>
#include <cmath>

typedef float T;

typedef struct Part_of_the_interval{
	T start;
	T sum;
	int count_step;
}p_interval;

void menu(T*, T*, int*, int*);
T function(T);
int integration(T, T, int, int);