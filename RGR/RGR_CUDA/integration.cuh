#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cstdio>
#include <iostream>
#include <stdio.h>
#include <cmath>
#include <ctime>

__host__ void menu(double*, double*, int*, int*);
__host__ int integration(double, double, int, int);