#include "integration.cuh"

__host__ int main(int argc, char ** argv) {
	double start, end;
	int n_thread, steps;
	if (argc == 5) {
		start = atof(argv[1]);
		end = atof(argv[2]);
		steps = std::atoi(argv[3]);
		n_thread = std::atoi(argv[4]);
		if (end <= start || n_thread<1 || steps < n_thread)
		{
			std::cout << "Error arguments!" << std::endl;
		}
	}
	else {
		menu(&start, &end, &n_thread, &steps);
	}
	integration(start, end, n_thread, steps);
	return 0;
}

__host__ void menu(double *start, double *end, int *n_thread, int*steps) {
	std::cout << "Enter the begining and end of the integration interval." << std::endl;
	while (1) {
		std::cout << "Input start:" << std::endl;
		std::cin >> *start;
		std::cout << "Input end:" << std::endl;
		std::cin >> *end;
		if (*end>*start) break;
		else std::cout << "Wrong interval!" << std::endl;
	}
	while (1) {
		std::cout << "Input count steps:" << std::endl;
		std::cin >> *steps;
		if (*steps>0) break;
	}
	while (1) {
		std::cout << "Input count threads:" << std::endl;
		std::cin >> *n_thread;
		if (*n_thread>0) break;
	}
}
