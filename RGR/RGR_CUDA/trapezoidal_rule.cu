#include "integration.cuh"

#define CUDA_DEBUG

#ifdef CUDA_DEBUG
#define CUDA_CHECK_ERROR(err) \
if(err != cudaSuccess)\
 std::cout << "Cuda error: " << cudaGetErrorString(err) << std::endl;\
 std::cout << "Error in file: " << __FILE__ << " Line: " << __LINE__ << std::endl;
#else
#define CUDA_CHECK_ERROR(err)
#endif


//������� ������� �������� ������� �� ����� (����� ��������)
__global__ void trapeziodal(double *start, double step, int *count_step, double *sum) {

	int index = blockIdx.x;
	for (int i = 0; i < count_step[index]; i++)
	{
		if (i == 0 || i == count_step[index] - 1) {//������� ��������
			sum[index] += 0.5*(sqrtf(start[index]) + start[index]*sinf(start[index]) + 2);
		}
		else {
			sum[index] +=sqrtf(start[index]) + start[index]*sinf(start[index]) + 2;
		}
		start[index] += step;
	}
	(sum[index]) *= step;
}

__global__ void sumReduce(double *g_idata, double *g_odata) {
	int index = threadIdx.x;
	for (unsigned int s = 1; s < blockDim.x; s *= 2) {
		if (index % (2 * s) == 0) {
			g_idata[index] += g_idata[index + s];
		}
	}

	if (index == 0)
	{
		g_odata[index] = g_idata[index];
	}
}

__host__ int integration(double start, double end, int n_thread, int count_step) {

	double step = (end - start) / count_step;//������ ����
	double *starts = (double*)calloc(n_thread, sizeof(double));
	double *sum = (double*)calloc(n_thread, sizeof(double));
	double *result = (double*)calloc(n_thread, sizeof(double));
	int *counts_step = (int*)calloc(n_thread, sizeof(int));
	float cuda_time;
	unsigned int start_time;
	unsigned int end_time;
	unsigned int time;
	start_time = clock();

	//������������� ���������� ��������� ����������
	int n = (int)(count_step / n_thread);
	for (int i = 0; i< n_thread; i++) {
		counts_step[i] = n;
		sum[i] = 0;
	}
	count_step %= n_thread;
	n = 0;
	while (count_step > 0) {
		counts_step[n++]++;
		count_step--;
	}
	//������ ��������� ������� � ������ �����
	starts[0] = start;
	for (int i = 1; i<n_thread; i++) {
		starts[i] = starts[i - 1] + counts_step[i - 1] * step;
	}

	int n_sumThread;
	for (n_sumThread = 1; n_sumThread <= n_thread; n_sumThread *= 2);

	
	cudaEvent_t event_cuda;
	cudaEventCreate(&event_cuda);

	//�������� ������ � �������� � device
	double *starts_d, *sum_d, *sum_all_d, *result_d;
	int *counts_step_d;
	CUDA_CHECK_ERROR(cudaMalloc((void**)&starts_d, sizeof(double)*n_thread));
	CUDA_CHECK_ERROR(cudaMalloc((void**)&counts_step_d, sizeof(int)*n_thread));
	CUDA_CHECK_ERROR(cudaMalloc((void**)&sum_d, sizeof(double)*n_thread));
	CUDA_CHECK_ERROR(cudaMalloc((void**)&sum_all_d, sizeof(double)*n_sumThread));
	CUDA_CHECK_ERROR(cudaMalloc((void**)&result_d, sizeof(double)*n_thread));
	CUDA_CHECK_ERROR(cudaMemcpy(starts_d, starts, sizeof(double)*n_thread, cudaMemcpyHostToDevice));
	CUDA_CHECK_ERROR(cudaMemcpy(sum_d, sum, sizeof(double)*n_thread, cudaMemcpyHostToDevice));
	CUDA_CHECK_ERROR(cudaMemcpy(counts_step_d, counts_step, sizeof(int)*n_thread, cudaMemcpyHostToDevice));

	//������ ������� ������ ������
	cudaEvent_t event_start, event_stop;
	CUDA_CHECK_ERROR(cudaEventCreate(&event_start));
	CUDA_CHECK_ERROR(cudaEventCreate(&event_stop));
	
	cudaEventRecord(event_start, 0);
	trapeziodal <<< 1, n_thread >>> (starts_d, step, counts_step_d, sum_d);
	cudaEventSynchronize(event_cuda);
	//�������� ��������� � ������� �� ����
	CUDA_CHECK_ERROR(cudaMemcpy(sum, sum_d, sizeof(double)*n_thread, cudaMemcpyDeviceToHost));

	double *sum_ex;
	if (n_thread != n_sumThread)
	{
		sum_ex = (double*)calloc(n_sumThread, sizeof(double));
		for (int i = 0; i < n_sumThread; i++)
		{
			if (i < n_thread)
			{
				sum_ex[i] = sum[i];
			}
		}
		CUDA_CHECK_ERROR(cudaMemcpy(sum_all_d, sum_ex, sizeof(double)*n_thread, cudaMemcpyHostToDevice));
	}
	else
	{
		CUDA_CHECK_ERROR(cudaMemcpy(sum_all_d, sum, sizeof(double)*n_thread, cudaMemcpyHostToDevice));
	}
	sumReduce <<< 1, n_sumThread >>> (sum_all_d, result_d);
	//cudaEventSynchronize(event_cuda);
	cudaEventRecord(event_stop, 0);

	cudaEventSynchronize(event_stop);
	cudaEventElapsedTime(&cuda_time, event_start, event_stop);
	CUDA_CHECK_ERROR(cudaMemcpy(result, sum_all_d, sizeof(double)*n_thread, cudaMemcpyDeviceToHost));
	end_time = clock();
	time = end_time - start_time;

	//������� ��������� ������� �� �����
	std::cout << "Result: " << *result << std::endl;
	std::cout << "Time: " << (double)time / CLOCKS_PER_SEC * 1000 << "ms" << std::endl;
	std::cout << "CUDA time: " << cuda_time << "ms" << std::endl;

	//������ ������� �� ����������
	CUDA_CHECK_ERROR(cudaFree(starts_d));
	CUDA_CHECK_ERROR(cudaFree(counts_step_d));
	CUDA_CHECK_ERROR(cudaFree(sum_d));
	CUDA_CHECK_ERROR(cudaFree(sum_all_d));
	CUDA_CHECK_ERROR(cudaFree(result_d));

	CUDA_CHECK_ERROR(cudaEventDestroy(event_cuda));
	CUDA_CHECK_ERROR(cudaEventDestroy(event_start));
	CUDA_CHECK_ERROR(cudaEventDestroy(event_stop));

	//������ ������ �� �����
	free(starts);
	free(counts_step);
	free(sum);
	free(result);
	free(sum_ex);
	system("pause");
	return 0;
}
