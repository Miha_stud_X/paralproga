#include <cstdio>
#include <iostream>
#include <cmath>
#include <unistd.h>
#include <wait.h>

typedef float T;

typedef struct Part_of_the_interval{
	T start;
	int count_step;
}p_interval;

void menu(T*, T*, int*, int*);
int integration(T, T, int, int);