#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <wait.h>
#include <unistd.h>

#define BUF_SIZE 255

int main() {
    int countTests = 0;
    char *argv[6];
    char *buf = (char*)malloc(sizeof(char)*BUF_SIZE);
    argv[0] = "../Process_integral";
    argv[1] = "Process_integral";
    for(int i = 0; i<4; i++){
        printf("Введите %dый аргумент: ", i+1);
        scanf("%s", buf);
        argv[i+2] = (char*)malloc(sizeof(char)*strlen(buf));
        strncpy(argv[i+2], buf, sizeof(argv[i+2]));
    }
    free(buf);
    printf("Введите число тестов: ");
    scanf("%d", &countTests);
    puts("Тест запущен:");
    pid_t pid;
    int i;
    long int sum = 0;
    struct timespec t_start, t_end;
    for (i = 0; i < countTests; i++) {
        clock_gettime(CLOCK_MONOTONIC, &t_start);
        if ((pid = fork()) == 0) {
            if (execl(argv[0],argv[1],argv[2],argv[3],argv[4],argv[5], NULL)){
                printf("\nОшибка запуска %d теста", i+1);
            }
            break;
        } else {
            wait(NULL);
            clock_gettime(CLOCK_MONOTONIC, &t_end);
            sum += 1000*(t_end.tv_sec-t_start.tv_sec)+(t_end.tv_nsec-t_start.tv_nsec)/1000000;
            if (i==countTests-1){
                printf("Среднее время работы программы: %ld мс", sum/countTests);
            }
        }
    }

    free(argv[2]);
    free(argv[3]);
    free(argv[4]);
    free(argv[5]);

    return 0;
}
