#include "integration.h"

#define TIME

T function(T x){
    return sqrtf(x)+x*sinf(x)+2;
}

void trapeziodal(p_interval interval, T step, int fd){
	T sum;
	for(int k = 0; k < interval.count_step; k++){
		if(k == 0 || k == interval.count_step-1) {//крайние значения
			sum += 0.5*function(interval.start);
		}else{
			sum += function(interval.start);
		}
		interval.start+=step;
	}
	sum*=step;
	write(fd, &sum, sizeof(T));
}


int integration(T start, T end, int n_proc, int count_step){
	pid_t pid[n_proc];//массив pid для процессов
	p_interval interval[n_proc];//массив интервалов для процессов
	T step = (end-start)/count_step;//размер шага
    int ost = count_step%n_proc;//остаток шагов для распределения
	int fd[2];//pipe discriptors
	pipe(fd);

#ifdef TIME	//Запись времени начала работы
    struct timespec t_start, t_end;
    long int delta_t;
	clock_gettime(CLOCK_REALTIME, &t_start);
#endif
	//распределение интервалов исходного промежутка
	int n = (int)(count_step/(n_proc));
	for (int i = 0; i< n_proc; i++){
		interval[i].count_step = n;
		if(ost>0){
            interval[i].count_step++;
            ost--;
		}
        if (i==0) interval[0].start = start;
        else interval[i].start = interval[i-1].start + interval[i-1].count_step*step;
	}

//--------------------------работа с процессами---------------------------//
	int i = 0;
	for(; i<n_proc;i++){//создание процессов
		if ((pid[i] = fork())==0){
			break;
		}
	}
	if (i==n_proc){//процесс родитель после создания суммирует результаты дочерних
		T all_sum=0;
		T *sum_for_1_interval;
		for(int i = 0; i<n_proc;i++){
			wait(NULL);
			read(fd[0], sum_for_1_interval, sizeof(T));
			all_sum += *sum_for_1_interval;
		}
#ifdef TIME
		clock_gettime(CLOCK_REALTIME, &t_end);
        delta_t=1000*(t_end.tv_sec-t_start.tv_sec)+(t_end.tv_nsec-t_start.tv_nsec)/1000000;
		std::cout << "Time: "<< delta_t << "мс" << std::endl;
#endif
        std::cout << "Result: "<< all_sum << std::endl;
   }else{
		//каждый процесс интегратор считает свою часть
		trapeziodal(interval[i], step, fd[1]);
	}
	return 0;
}
