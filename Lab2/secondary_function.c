#include "potocs.h"
#include <stdlib.h>

int push_queue(Node_t **head, T value){
    Node_t *tmp = malloc(sizeof(Node_t));
	if (tmp == NULL) {
		return 0;
    }
    tmp->next = NULL;
    tmp->value = value;
    if(*head){
		Node_t *buf_head = *head;
		while(buf_head->next != NULL){
			buf_head = buf_head->next;
		}
		buf_head->next = tmp;
	}else{
		*head = tmp;
	}
    return 1;
}

Node_t *create_queue(T value){
    Node_t *tmp = malloc(sizeof(Node_t));
	if (tmp == NULL) {
		return NULL;
    }
    tmp->next = NULL;
    tmp->value = value;
	return tmp;
}

int pop(Node_t **head){
    if ((*head) == NULL) {
        return 0;
    }
    Node_t *val = *head;
    *head = (*head)->next;
    free(val);
    return 1;
}

T peek(const Node_t* head){
    return head->value;
}

int getSize(const Node_t *head){
    int size = 0;
    while (head) {
        size++;
        head = head->next;
    }
    return size;
}
