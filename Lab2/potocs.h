#define NAME_MAX 30

typedef int T;
typedef struct Node_tag {
    T value;
    struct Node_tag *next;
} Node_t;

typedef struct struct_to_potoc {
    int number;
    struct Node_tag *head;
} STP;

int take_potocs();
void *func(void *);
void *thread_print(void *);
void menu_in();
void consolIN();
void filelIN();
void generateIN();

//работа с очередью
int pop(Node_t **);
T peek(const Node_t *);
int getSize(const Node_t *);
int push_queue(Node_t **, T);
Node_t *create_queue(T);
