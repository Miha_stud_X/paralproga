#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "potocs.h"

pthread_mutex_t mutex;
Node_t *queueIN;//очередь входных данных

int take_potocs(){
	int threadsCount = 0;//число пар потоков
	queueIN = NULL;//очередь входных данных
	//Структуры для сохранения определенного времени
    struct timespec t_start, t_end; 
    //Переменная для расчета дельты времени
    long int delta_t;
	while (threadsCount < 1){
		puts("Введите число пар потоков:");
		scanf("%d", &threadsCount);//считывание числа пар потоков
	}
	STP *queuesOut[threadsCount];//создание структур с номером и очередью
	for(int i = 0; i<threadsCount; i++){
		//выделение памяти обнуление выходных очередей
		queuesOut[i] = malloc(threadsCount*sizeof(STP));
		(queuesOut[i])->head = NULL;
		(queuesOut[i])->number = i+1;
	}
	pthread_t tid[threadsCount*2];//массив tid для потоков
	menu_in();//вызов меню
	clock_gettime(CLOCK_REALTIME, &t_start);//Определяем текущее время
	pthread_mutex_init(&mutex, NULL);//инициализация мьютека
	for (int i = 0; i<threadsCount; i++){
		pthread_create(&tid[i],NULL,func, &(queuesOut[i]->head));
		pthread_create(&tid[i+threadsCount],NULL,thread_print,queuesOut[i]);
	}
	for (int i = 0; i < threadsCount*2; i++){
		pthread_join(tid[i], NULL);
	}
	pthread_mutex_destroy(&mutex);//разрушение мьютека
	//вывод времени работы всей программы
	clock_gettime(CLOCK_REALTIME, &t_end);
	delta_t=1000000000*(t_end.tv_sec-t_start.tv_sec)+
								(t_end.tv_nsec-t_start.tv_nsec);
	printf("Время работы программы: %ld нс\n",delta_t);
	return 0;
}

void *func(void *ptc){
	Node_t **queueOut = ptc;
	int div = 2;
	T bufValue;
	while(queueIN != NULL){//пока во входном списке что-то есть
		//взятие значения из общего списка
		pthread_mutex_lock(&mutex);
			if(queueIN){
				bufValue = peek(queueIN);
				pop(&queueIN);
			}else {//на случай, если во время ожидания мьютекса 
				   //список закончился
				pthread_mutex_unlock(&mutex);
				break;
			};
		pthread_mutex_unlock(&mutex);
		//запись самого числа в список для вывода
		push_queue(queueOut, bufValue);
		//расчёт и запись результата в список для вывода
		while (bufValue > 1){
			while (bufValue % div == 0){
				push_queue(queueOut, div);
				bufValue /= div;
			}
			div+=div==2?1:2;
		}
		push_queue(queueOut,0);//запись в список 0 как окончания записи
		div = 2;
	}
	//запись в список -1 как окончания данных
	push_queue(queueOut, -1);
	return NULL;
}

void *thread_print(void *ptc){
	STP *queueOut = ptc;
	T bufToPrint;
	int start_flag = 0;
	
	FILE *file;
	char nameFile[] = {(char)(queueOut->number+'0'),'.','t','x','t'};
	remove(nameFile);
	
	while(1){
		if ((queueOut->head) != NULL){
			bufToPrint = peek(queueOut->head);
			if (bufToPrint < 0)//если данные закончились 
				break;
			file = fopen(nameFile,"a");
			if(bufToPrint == 0){//если конец записи
				fprintf(file, "\n");
				start_flag = 0;
			}else{
				if (start_flag){//если множитель
					fprintf(file, " * %d", bufToPrint);
				}else{//если исходное значение
					fprintf(file, "%d = 1", bufToPrint);
					start_flag = 1;
				}
			}
			fclose(file);
			pop(&(queueOut->head));
		}
	}
	return NULL;
}

void menu_in(){
	int buf;
	do{
	puts("\n1.Ввести данные\n\
		\r2.Загрузить из файла\n\
		\r3.Сгенирировать случайные");
	scanf("%d", &buf);
		switch(buf){
			case 1://ввод данных с консоли
				consolIN();
				break;
			case 2://ввод данных из файла
				filelIN();
				break;
			case 3:
				generateIN();//генерация чисел
				break;
			default:
				puts("такого пункта меню нет");
				break;
		}
	}while(queueIN==NULL);
}

void consolIN(){
	T buf;
	puts("\nВведите числа для расчётов\n\
			\r(закончить ввод числом 0)");
	while(1){
		scanf("%d", &buf);
		if (buf!=0){ 
			push_queue(&queueIN, buf);
		}else break;
	}			
}

void filelIN(){
	T buf;
	char fileName[NAME_MAX], adres[]="file_in/";
	FILE *file;
	puts("Введите имя файла:");
	scanf("%s", fileName);
	file = fopen(strcat(adres, fileName), "r");
	while(1){
		fscanf(file, "%d", &buf);
		if (buf!=0){ 
			push_queue(&queueIN, buf);
		}else break;
	}
}

void generateIN(){
	T buf, maxIn;
	puts("Введите кол-во чисел:");
	scanf("%d", &buf);
	puts("Введите маскимальное значение:");
	scanf("%d", &maxIn);
	for(int i = buf; i>0; i--){
		buf = rand() % maxIn + 1;
		push_queue(&queueIN, buf);
	}
}
