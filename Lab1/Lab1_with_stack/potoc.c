#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include "potocs.h"

//структура значения в стеке
typedef struct koord{
	int number;
	double x;
}koord;

//структура стека значений для потока
typedef struct stack_thread{
	koord *n;
	int l;
}s_t;

//функция возведения в степень
double pow(double x, double y){
	double rez=1;
	for (int i = 0; i<y; i++)
		rez*=x;
	return rez;
	/* вообще хз на сколько верно вызывать 
	 * из разных потоков одну функцию,
	 * но вроде всё норм работает
	 */
}

//функция расчёта заданной функции в точках, положенных в стек потока
void *func(void *ptc){
	s_t *ptc1 = ptc;
	for (int t = 0; t<ptc1->l; t++){
		(ptc1->n+t)->x=pow((ptc1->n+t)->x,8)/(pow((ptc1->n+t)->x,3)+1);
	}
	return 0;
}

int take_potocs(){
	int n_thread=0, steps;
	double borders[2];
	printf("Введите число потоков:\n");
	scanf("%d", &n_thread);
	printf("\nВведите границы интегрирования:\n");
	scanf("%lf %lf", &borders[0], &borders[1]);
	printf("\nВведите кол-во шагов:\n");
	scanf("%d", &steps);
	printf("\n");
	
	pthread_t tid[n_thread];
	
	double step = (borders[1]-borders[0])/steps;//шаг
	
	s_t mass[n_thread];//массив стеков значений для потоков
	
	//равномерное распределение точек по стекам потоков
	int l = (n_thread>steps+1?steps+1:n_thread);
	for (int i = 0; i<l; i++){
		mass[i].n=(koord*)malloc(sizeof(koord));
		mass[i].n->number=i+1;
		mass[i].l=1;
		mass[i].n->x=step*i+borders[0];
	}
	l = 0;
	int k = n_thread;
	for (int i = 0; i<steps+1-n_thread; i++){
		if (l==n_thread) l = 0;
		mass[l].n=(koord*)realloc(mass[l].n,(mass[l].l+1)*sizeof(koord));
		(mass[l].n+mass[l].l)->number=k;
		(mass[l].n+mass[l].l)->x=step*k+borders[0];
		mass[l].l++;
		l++;
		k++;
	}
	//
	//создание потоков
	for (int i = 0; i<n_thread; i++){
		pthread_create(&tid[i], NULL, func, &mass[i]);
	}
	//
	//закрытие потоков(привязка)
	for (int i = 0; i<n_thread; i++){
		pthread_join(tid[i], NULL);
	}
	//
	//вывод
	printf("Ты мне формулу не скинул, но вот полученные значения в точках:\n");
	for (int i = 0; i<n_thread; i++){
		for (int t = 0; t<mass[i].l; t++)
			printf("%d:%lf\n",(mass[i].n+t)->number, (mass[i].n+t)->x);
	}
	//
	//очистка памяти
	for (int i = 0; i<n_thread; i++)
		free(mass[i].n);
	//
	return 0;
}
