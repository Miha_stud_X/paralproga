#include <stdio.h>
#include <pthread.h>
#include "potocs.h"

double stap;//размер шага

//структура куска для потока
typedef struct kusok{
	double start;
	double sum;
	int steps;
}kusok;

//функция расчёта заданной функции на куске (метод трапеций)
void *func(void *ptc){
	kusok *ptc1 = ptc;
	for(int i = 0; i<(ptc1->steps)+1; i++)
	{
		if(i==0 || i==ptc1->steps) {//крайние значения
			ptc1->sum += 0.5*((pow(ptc1->start,10)+20*pow(ptc1->start,9)
			+pow(ptc1->start,8)-10*pow(ptc1->start,7)+8*pow(ptc1->start,6)
			+45*pow(ptc1->start,5)+10*pow(ptc1->start,4)) / 
			(pow(ptc1->start,7)+15*pow(ptc1->start,4)));
		}else{
			ptc1->sum += ((pow(ptc1->start,10)+20*pow(ptc1->start,9)
			+pow(ptc1->start,8)-10*pow(ptc1->start,7)+8*pow(ptc1->start,6)
			+45*pow(ptc1->start,5)+10*pow(ptc1->start,4)) / 
			(pow(ptc1->start,7)+15*pow(ptc1->start,4)));
		}
		ptc1->start+=stap;
	}
	ptc1->sum*=stap;
	return 0;
}

int take_potocs(){
	int n_thread, steps;
	double borders[2];
	printf("Введите число потоков:\n");
	scanf("%d", &n_thread);//считали число потоков
	printf("\nВведите границы интегрирования:\n");
	scanf("%lf %lf", &borders[0], &borders[1]);//границы
	printf("\nВведите кол-во шагов:\n");
	scanf("%d", &steps);//число шагов
	printf("\n");
	
	pthread_t tid[n_thread];//массив tid для потоков
	kusok mass[n_thread];//массив кусков для потоков
	stap = (borders[1]-borders[0])/steps;//размер шага
	//
	//Структуры для сохранения определенного времени
    struct timespec t_start, t_end; 
    //Переменная для расчета дельты времени
    long int delta_t;
    //Определяем текущее время
	clock_gettime(CLOCK_REALTIME, &t_start);

	//начальная инициализация
	for (int i = 0; i<n_thread; i++){
		mass[i].steps = 0;
		mass[i].sum = 0;
	}
	//
	//распределение кусков исходного промежутка
	int k = 0;
	while(steps>0){
		mass[k++].steps++;
		steps--;
		if (k == n_thread) k = 0;
	}
	//
	//запись начальной границы в каждый кусок
	mass[0].start = borders[0];
	for(int i = 1; i<n_thread; i++){
		mass[i].start = mass[i-1].start + mass[i-1].steps*stap;
	}
	//
	//создание потоков
	for (int i = 0; i<n_thread; i++){
		pthread_create(&tid[i], NULL, func, &mass[i]);
	}
	//
	//закрытие потоков(привязка)
	for (int i = 0; i<n_thread; i++){
		pthread_join(tid[i], NULL);
	}
	//
	//вывод суммы значений
	double all_sum=0;
	for (int i = 0; i<n_thread; i++){
		all_sum+=mass[i].sum;
	}
	printf("Результат: %lf\n", all_sum);
	//
	//вывод времени работы всей программы
	clock_gettime(CLOCK_REALTIME, &t_end);//Определяем текущее время
	//Рассчитываем разницу времени между двумя измерениями
	delta_t=1000000000*(t_end.tv_sec - t_start.tv_sec)+(t_end.tv_nsec - t_start.tv_nsec);
	//Выводим результат расчета на экран
	printf("Время работы программы: %ld нс\n",delta_t);
	//
	return 0;
}
